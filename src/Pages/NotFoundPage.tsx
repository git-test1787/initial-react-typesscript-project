/** @jsx jsx */
import { jsx } from '@emotion/core';
import { Button, Grid } from '@material-ui/core';
import React from 'react';
import { Link } from 'react-router-dom'

export const NotFoundPage: React.FunctionComponent = (
): JSX.Element => {
    return (<React.Fragment>
        <div style={{ height: '100%', width: '100%' }}>
            <Grid
                container
                direction="row"
                justify="center"
                alignItems="center"
            >
                <img src="https://www.pngpix.com/wp-content/uploads/2016/10/PNGPIX-COM-Cowboy-Vector-PNG-Image.png" style={{height: '15rem', width: '15rem'}}/>
                <span style={{fontWeight: 600}}>Uhhhhhhh! Wrong Page </span><Button size="small" style={{ color: 'white' }} component={Link} to="/login" >
                                    Click here to Login
                        </Button>
            </Grid>
        </div>
    </React.Fragment>)
}