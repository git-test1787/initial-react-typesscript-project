/** @jsx jsx */
import { jsx } from '@emotion/core';
import { Button, Grid } from '@material-ui/core';
import React from 'react';
import { useHistory } from "react-router-dom";

export const NetworkIssue: React.FunctionComponent = (
): JSX.Element => {
    const history = useHistory();

    const handleClick = () => {
       
    }
    
    return (<React.Fragment>
        <div style={{ height: '100%', width: '100%' }}>
            <Grid
                container
                direction="row"
                justify="center"
                alignItems="center"
            >
                <img src="http://pngimg.com/uploads/bear/bear_PNG1201.png" style={{height: '15rem', width: '15rem'}}/>
                <br/>
                <span style={{fontWeight: 600}}>Seems Like! It's Network Issue </span><Button size="small" onClick={handleClick} style={{ color: 'white' }} >
                                    Click here to Refresh
                        </Button>
            </Grid>
        </div>
    </React.Fragment>)
}