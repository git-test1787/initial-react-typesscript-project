/** @jsx jsx */
import { css, jsx } from '@emotion/core';
import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { BrowserRouter } from "react-router-dom";

export const FullContent = css`
    width: 100%;
    height: 100%;
    min-height: 100%;
`;

const RenderPage = () => {
    return (
        <App />
    )
}

ReactDOM.render(<BrowserRouter>
    <React.Fragment>
        <RenderPage />
    </React.Fragment></BrowserRouter>, document.getElementById("root"));
