/** @jsx jsx */
import { css, jsx } from '@emotion/core';
import React, { useEffect, useState } from "react";
import { Route, HashRouter as Router, Switch } from 'react-router-dom';
import { LoginPage } from "./Pages";
import { NotFoundPage } from './Pages/NotFoundPage';

const App = () => {
    return (<React.Fragment>
        <Router>
            <div>
                <Switch>
                    <Route path="/login" component={LoginPage} />
                    <Route path="/*" component={NotFoundPage} />
                </Switch>
            </div>
        </Router>
    </React.Fragment>)
}

export default App;
