"use strict";
const HTMLWebpackPlugin = require("html-webpack-plugin");
const path = require("path");


module.exports = {
  mode: "production",
  externals: {
    'Config': JSON.stringify(process.env.NODE_ENV === 'production' ? {
      serverUrl: "http://15.206.153.243:3000"
    } : {
      serverUrl: "http://15.206.153.243:3000"
    })
  },
  plugins: [
    new HTMLWebpackPlugin({
      filename: "./index.html",
      template: path.join(__dirname, 'public/index.html')
    })
    ],
  
  // Where to compile the bundle
  // By default the output directory is `dist`
  output: {
    filename: "bundle.js",
    path: path.resolve(__dirname, 'dist'),
  },

  // Supported file loaders
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: "ts-loader"
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader",
            options: { minimize: true }
          }
        ]
      },
    ]
  },
  optimization: {
    sideEffects: false
  },
  // File extensions to support resolving
  resolve: {
    extensions: [".ts", ".tsx", ".js"]
  },

  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 9000
  },
};
